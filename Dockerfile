FROM ubuntu:latest

RUN \
# Update
apt-get update -y && \
# Install Java
apt-get install default-jre -y

ADD ./target/spring-boot-rest-2-0.0.1-SNAPSHOT.jar micro-services.Application.jar

EXPOSE 8083

CMD java -jar micro-services.Application.jar